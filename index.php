
<?php
ini_set('display_errors','Off');


if (isset($_REQUEST['formValue'])){

    $userCity = $_GET['formValue'];
    $appid = 'de111201b6dcb18b82bfe0af1537f73f';

    $url = ("http://api.openweathermap.org/data/2.5/weather?q={$userCity}&units=metric&appid={$appid}");

    $weatherData = file_get_contents($url);
    $weatherResult = json_decode($weatherData, 1);
    $cityId = $weatherResult['id'];
    $coordLon = $weatherResult['coord']['lon'];
    $coordLat = $weatherResult['coord']['lat'];
    $cloud = $weatherResult['weather']['0']['description'];
    $temp = $weatherResult['main']['temp'];
    $pressure = $weatherResult['main']['pressure'];
    $humidity = $weatherResult['main']['humidity'];
    $pressure = round(($pressure / 133.33)*100);
    $windSpeed = $weatherResult['wind']['speed'];
    $weatherIcon = $weatherResult['weather'][0]['icon'];
    $weatherIconUrl = 'http://openweathermap.org/img/w/' . $weatherIcon . '.png';

    $error = "Названия Российских городов можно вводить кириллицей, остальных только латиницей";
}
 ?>





<!DOCTYPE html>
<html>
<head>
    <title>Погодка</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <style type="text/css">
        /* CLIENT-SPECIFIC STYLES */
        body, table, td, a{-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
        table, td{mso-table-lspace: 0pt; mso-table-rspace: 0pt;} /* Remove spacing between tables in Outlook 2007 and up */
        img{-ms-interpolation-mode: bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */

        /* RESET STYLES */
        img{border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;}
        table{border-collapse: collapse !important;}
        body{height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important;}

        /* iOS BLUE LINKS */
        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /* MOBILE STYLES */
        @media screen and (max-width: 750px) {

            /* ALLOWS FOR FLUID TABLES */
            .wrapper {
                width: 100% !important;
                max-width: 100% !important;
            }

            .img-max {
                max-width: 100% !important;
                width: 100% !important;
                height: auto !important;
            }

            /* FULL-WIDTH TABLES */
            .responsive-table {
                width: 100% !important;
            }

            .responsive-table__cell {
                width: 100% !important;
                max-width: 520px !important;
                display: block !important;
            }

            .responsive-table__cell--2 {
                padding-right: 0 !important;
            }
        }

        body {
            font-family: Helvetica, Arial, sans-serif;
        }

        input {

            border-radius: 50px;
            border-color: #FF6E68;
            border-style: groove;
            margin-top: 35px;
            background: transparent;
            width: 80%;
            min-width: 50%;
            max-width: 85%;
            text-align: center;
            font: 25px 'Arial', sans-serif;
            color: #FF6E68;
            padding: 15px 30px 15px 30px;
            outline: none;



        }

        button {

            border-radius: 50px;
            border-color: #FF6E68;
            border-style: groove;
            margin-top: 20px;
            background: transparent;
            width: 80%;
            min-width: 50%;
            max-width: 100%;
            text-align: center;
            font: 18px 'Arial', sans-serif;
            color: #FFE600;
            padding: 15px 30px 15px 30px;
            outline: none;
            margin-bottom: 50px;
        }
        button:hover { background: rgb(232,95,76); } /* при наведении курсора мышки */
        button:active { background: rgb(152,15,0); } /* при нажатии */


        .lead {

        }

        .mobile-left {
            text-align: left !important;
        }

        .lead__subcopy {
            font-family: Helvetica, Arial, sans-serif;
            font-size: 10px;
            line-height: 1.4; /* 14px */
            color: #FF6E68;
            letter-spacing: 0.08em;
        }

        .lead__subcopy td {
            vertical-align: top;
        }

        .lead__headline {
            font-family: Helvetica, Arial, sans-serif;
            font-size: 50px;
            line-height: 1; /* 50px */
            font-weight: 700;
            color: #FF6E68;
        }

        .lead__text {
            font-family: Helvetica, Arial, sans-serif;
            font-size: 18px;
            line-height: 1.444444444; /* 26px */
            color: #FF6E68;
        }

        .lead__squiggle {
            background-image: url("https://newsletter-1.theoutline.com/v1/squiggle_2x.png");
            background-position: center center;
            background-size: auto 5px;
            max-width: 344px;
            width: 100%;
            height: 5px;
            margin: 0 auto;
        }

        .sentence-module__table {
            border-collapse: collapse;
        }

        .sentence-module__table td {
            border: 1px solid #FF6E68;
        }

        .sentence-module__headline {
            font-family: Helvetica, Arial, sans-serif;
            font-size: 50px;
            line-height: 1; /* 50px */
            font-weight: 700;
            color: #FF6E68;
        }

        .sentence-module__item__text a {
            font-family: Helvetica, Arial, sans-serif;
            font-size: 18px;
            line-height: 1.444444444; /* 26px */
            text-decoration: none;
            color: #FFFFFF;
        }

        .footnote,
        .social {
            font-family: Helvetica, Arial, sans-serif;
            font-size: 18px;
            line-height: 1.444444444; /* 26px */
            color: #FF6E68;
        }

        .footnote a {
            color: #FFFFFF;
            text-decoration: underline;
        }

        .footer {
            font-family: Helvetica, Arial, sans-serif;
            font-size: 12px;
            line-height: 1.666666667; /* 20px */
            color: #FF6E68;
        }

        .footer a {
            color: #FF6E68;
            text-decoration: underline;
        }

        .social__links a {
            color: #FF6E68;
            text-decoration: none;
            margin-right: 30px;
        }

        .social__links .last {
            margin-right: 0px;
        }

        .responsive-table {
            table-layout: fixed;
        }

        .responsive-table__cell {
            vertical-align: top;
        }

        .responsive-table--thirds .responsive-table__cell--2 {
            padding-right: 40px;
        }

        .grid-item--linkout {
            position: relative;
        }

        .grid-item__title {
            font-family: Helvetica, Arial, sans-serif;
            font-size: 32px;
            line-height: 1; /* 32px */
            font-weight: 700;
            color: #FFFFFF;
            margin: 0; /* reset for h2 */
        }

        .grid-item__data {
            font-family: Helvetica, Arial, sans-serif;
            font-size: 32px;
            line-height: 1; /* 32px */
            font-weight: 700;
            margin: 0; /* reset for h2 */
        }

        .grid-item__data-text-size-xsmall {
            font-size: 68px;
        }

        .grid-item__data-text-size-small {
            font-size: 84px;
        }

        .grid-item__data-text-size-medium {
            font-size: 100px;
        }

        .grid-item__data-text-size-large {
            font-size: 116px;
        }

        .grid-item__data-text-size-xlarge {
            font-size: 132px;
        }

        .grid-item__source {
            position: absolute;
            left: 20px;
            bottom: 20px;
        }

        .grid-item__source__link {
            font-family: Helvetica, Arial, sans-serif;
            font-size: 14px;
            line-height: 1; /* 14px */
            font-weight: 700;
            text-decoration: none;
            text-transform: uppercase;
            color: #2C185B;
        }

        @media screen and (max-width: 750px) {
            .desktop-only {
                display: none;
            }

            div.mobile-only {
                display: block !important;
                max-height: none !important;
                font-size: 16px !important;
            }

            .lead__headline {
                font-size: 32px;
            }

            .sentence-module {
                padding-left: 0px !important;
                padding-right: 0px !important;
            }

            .sentence-module__headline {
                font-size: 32px;
                text-align: left;
                border-left: 0px !important;
                border-right: 0px !important;
            }

            .sentence-module__item__outmoji {
                padding-top: 10px !important;
                padding-bottom: 10px !important;
                padding-left: 5px !important;
                padding-right: 5px !important;
                border-left: 0px !important;
            }

            .sentence-module__item__text {
                border-right: 0px !important;
            }

            .sentence-module__item__outmoji img {
                width: 70px;
                height: 70px;
            }

            .social__inner {
                padding-top: 45px !important;
                padding-bottom: 25px !important;
            }

            .social__links {
                padding-top: 47px !important;
            }

            .footer__text {
                padding-top: 25px !important;
            }
        }

        @media screen and (min-width: 751px) {
            .responsive-table__cell {
                display: table-cell !important;
            }

            .desktop-center {
                text-align: center !important;
            }

            .desktop-center img {
                margin-left: auto;
                margin-right: auto;
            }

            .mobile-only {
                display: none;
            }


        }
    </style>
</head>
<body style="margin: 0 !important; padding: 0 !important;">

<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    </div>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td bgcolor="#2C185B" align="center" class="lead" style="padding: 0 20px 0 20px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" align="center" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px; text-align: center;" class="wrapper">
                <tr>
                    <td>
                        <table border="0" align="center" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <div class="desktop-only">
                                        <table border="0" align="center" cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr class="lead__subcopy">
                                                <td align="left" style="width: 33.333%; color: #FF6E68; padding-top: 40px;"></td>
                                                <td align="center" style="width: 33.333%; padding: 60px 0 0 0;">
                                                    <img src="https://newsletter-1.theoutline.com/v1/logo-pink.png" width="80" height="78" style="display: block; width: 80px; height: 78px; margin: 0 auto;" alt="The Outline" border="0">
                                                </td>
                                                <td align="right" style="width: 33.333%; color: #FF6E68; padding-top: 40px;"></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <!--[if !mso 9]><!-->
                                    <div class="mobile-only" style="mso-hide: all; display: none; max-height: 0px; overflow: hidden;">
                                        <table border="0" align="center" cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr class="lead__subcopy">
                                                <td align="left" style="padding-top: 20px;">
                                                    <img src="https://newsletter-1.theoutline.com/v1/logo-pink.png" width="80" height="78" style="display: block; width: 80px; height: 78px;" alt="The Outline" border="0">
                                                </td>
                                                <td align="right" style="color: #FF6E68; padding-top: 20px;">
                                                    <br>
                                                    &mdash;<br>
                                                                                                       </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--<![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td class="mobile-left desktop-center" align="center" style="padding: 40px 0 40px 0;">

                                    <h1 class="lead__headline" style="margin: 0; color: #FF6E68;">УЗНАЙ КАКАЯ ПОГОДА В ТВОЁМ ГОРОДЕ</h1>

                                    <form spx" action="index.php">
                                        <input class="form" type="value" placeholder="Название города" name="formValue" required><br>
                                        <button class="button" type="confirm">Поcмотреть погоду</button>
                                    </form>


                                    <div style="font-size: 20px; color: #FFE600"><?php
                                        if (empty($cityId)){
                                            echo "<p><b>$error</b></p>";
                                        }
                                        else{
                                        include 'wethdata.php';

                                        }
                                        ?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="mobile-left desktop-center" align="center" style=" padding: 0 0 40px 0;">
                                    <div class="lead__squiggle"></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="mobile-left desktop-center" align="center" style="padding: 0 0 40px 0;">
                                    <p class="lead__text" style="margin: 0; color: #FF6E68;">Немного полезных советов и фактов</p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>

    <tr>
        <td bgcolor="#2C185B" align="center" class="grid">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="750">
                <tr>
                    <td align="center" valign="top" width="750">
            <![endif]-->
            <table border="0" align="center" cellpadding="0" cellspacing="0" width="100%" style="max-width: 750px; text-align: center;" class="wrapper">
                <tbody>
                <tr align="center">
                    <td bgcolor="#FFE600" class="responsive-table__cell" style="width: 100%; max-width: 375px; display: block;">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" style="padding: 20px 20px 20px 20px;">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td align="center" valign="middle" style="padding: 0px 0px 20px 0px;">

                                                    <img src="https://asset-1.theoutline.com/v1/duotone/preview?end=FFE600&amp;start=2C185B&amp;url=https%3A%2F%2Foutline-prod.imgix.net%2F20171116-4rlnIuUfy19WvASsZYTH%3Ffit%3Dcrop%26h%3D189%26w%3D355%26s%3Ddd2c917f7c89ce406fd52fff8aee0ce6" width="335" height="189" style="display: block; width: 335px; height: 189px;" border="0" class="img-max" alt="">

                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="padding: 0px 0px 0px 0px;">
                                                <h2 class="grid-item__title">

                                        <span style="color: #2C185B">— ДАЖЕ МЁРТВЫE ВОССТАЮТ</span>
                                                        <span style="color: #FF6E68"> —КОГДА ЮЗНАЮТ, ЧТО НА УЛИЦЕ ОТЛИЧНАЯ ПОГОДКА.</span>

                                                </h2>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td bgcolor="#FF6E68" class="responsive-table__cell" style="width: 100%; max-width: 375px; display: block;">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" style="padding: 20px 20px 20px 20px;">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td align="left" valign="middle" style="padding: 0px 0px 20px 0px;">

                                                    <img src="https://asset-1.theoutline.com/v1/duotone/preview?end=FF6E68&amp;start=2C185B&amp;url=https%3A%2F%2Foutline-prod.imgix.net%2F20171120-ZmhkwAvN2vnwZLpfn7J5%3Ffit%3Dcrop%26h%3D189%26w%3D355%26s%3D354f2a0e303a24624e6bbf2fbd3dba7a" width="335" height="189" style="display: block; width: 335px; height: 189px;" border="0" class="img-max" alt="">

                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="padding: 0px 0px 0px 0px;">
                                                <h2 class="grid-item__title">

                                                        <span style="color: #2C185B">— УЗНАЛ КАКАЯ ЧУДЕСНАЯ ПОГОДКА</span>
                                                        <span style="color: #FFFFFF">— СКОРЕЕ РАССКАЖИ ОСТАЛЬНЫМ</span>

                                                </h2>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>

                </tr>
                <tr align="center">
                    <td bgcolor="#2C185B" class="responsive-table__cell" style="width: 100%; max-width: 375px; display: block;">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" style="padding: 20px 20px 20px 20px;">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td align="left" valign="middle" style="padding: 0px 0px 20px 0px;">

                                                    <img src="https://asset-1.theoutline.com/v1/duotone/preview?end=FFE600&amp;start=2C185B&amp;url=https%3A%2F%2Foutline-prod.imgix.net%2F20171120-KpIPLjLHyzCRXzBFVEpJ%3Ffit%3Dcrop%26h%3D189%26w%3D355%26s%3D69ada1e543a7e3b88e7343eb97d5bf4d" width="335" height="189" style="display: block; width: 335px; height: 189px;" border="0" class="img-max" alt="">

                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="padding: 0px 0px 0px 0px;">
                                                <h2 class="grid-item__title">

                                                        <span style="color: #FFE600">— НАРОДНЫЕ НЕДОВОЛЬСТВА НЕ ПРОБЛЕМА</span>
                                                        <span style="color: #FFFFFF">— НИКТО НЕ ВЫЙДЕТ НА УЛИЦУ В ПЛОХУЮ ПОГОДУ</span>

                                                </h2>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td bgcolor="#FFFFFF" class="responsive-table__cell" style="width: 100%; max-width: 375px; display: block;">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" style="padding: 20px 20px 20px 20px;">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td align="left" valign="middle" style="padding: 0px 0px 20px 0px;">

                                                    <img src="https://asset-1.theoutline.com/v1/duotone/preview?end=FFFFFF&amp;start=FF6E68&amp;url=https%3A%2F%2Foutline-prod.imgix.net%2F20171115-cRDPeIuR9Q1VcXlEjRQC%3Ffit%3Dcrop%26h%3D189%26w%3D355%26s%3D168af55eb8d281f68e89ef88dd00e99a" width="335" height="189" style="display: block; width: 335px; height: 189px;" border="0" class="img-max" alt="">

                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="padding: 0px 0px 0px 0px;">
                                                <h2 class="grid-item__title">

                                                        <span style="color: #FF6E68">— АЛЛЕРГИЯ НЕ ПРЕГРАДА</span>
                                                        <span style="color: #2C185B">— КОГДА ТЫ ЗНАЕШЬ ЧТО НА УЛИЦЕ ХОРОШЕЧНО</span>

                                                </h2>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>

                </tr>
                <tr align="center">
                    <td bgcolor="#FFE600" class="responsive-table__cell" style="width: 100%; max-width: 375px; display: block;">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" style="padding: 20px 20px 20px 20px;">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td align="left" valign="middle" style="padding: 0px 0px 20px 0px;">

                                                    <img src="https://asset-1.theoutline.com/v1/duotone/preview?end=FFE600&amp;start=2C185B&amp;url=https%3A%2F%2Foutline-prod.imgix.net%2F20171121-rE4TOqlvycH24JPapDvL%3Ffit%3Dcrop%26h%3D189%26w%3D355%26s%3D0989d8e86932c7f5b7fcfbc6bbb5381a" width="335" height="189" style="display: block; width: 335px; height: 189px;" border="0" class="img-max" alt="">

                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="padding: 0px 0px 0px 0px;">
                                                <h2 class="grid-item__title">

                                                        <span style="color: #2C185B">— ЧАРЛЬЗ МЭНСОН ВСЕГДА СЛЕДИЛ ЗА ПОГОДОЙ</span>
                                                        <span style="color: #FF6E68">— В ОТЛИЧИЕ ОТ СВОИХ ЖЕРТВ!</span>

                                                </h2>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td bgcolor="#FF6E68" class="responsive-table__cell" style="width: 100%; max-width: 375px; display: block;">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" style="padding: 20px 20px 20px 20px;">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td align="left" valign="middle" style="padding: 0px 0px 20px 0px;">

                                                    <img src="https://asset-1.theoutline.com/v1/duotone/preview?end=FF6E68&amp;start=2C185B&amp;url=https%3A%2F%2Foutline-prod.imgix.net%2F20171120-z90YPBL0B7HtDsgBrLJJ%3Ffit%3Dcrop%26h%3D189%26w%3D355%26s%3Df29d9e940b08b734d34c64532fb30cbc" width="335" height="189" style="display: block; width: 335px; height: 189px;" border="0" class="img-max" alt="">

                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="padding: 0px 0px 0px 0px;">
                                                <h2 class="grid-item__title">

                                                        <span style="color: #2C185B">— БУДЬ КАК ХЭНК ХИЛЛ</span>
                                                        <span style="color: #FFFFFF">— УСТРАИВАЙ В ПРЕКРАСНУЮ ПОГОДКУ БАРБЕКЮ</span>

                                                </h2>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>

</body>
</html>


